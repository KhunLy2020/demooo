﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DemoOO_Delegate
{
    abstract class Monstre
    {
        public int Niveau { get; set; }

        public abstract void Monter();
    }
}
