﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DemoOO_Delegate
{
    delegate void MonterNiveau();
    class Game
    {
        public List<Monstre> Monstres { get; set; }
            = new List<Monstre>();

        public event MonterNiveau MonterNiveauHandler;

        public Game()
        {
            Task.Run(() => {
                bool stop = false;
                while(!stop)
                {
                    if(DateTime.Now > new DateTime(2020,5,5,14,27,0))
                    {
                        MonterNiveauHandler.Invoke();
                        //MonterNiveauHandler();
                        stop = true;
                        foreach (Monstre m in Monstres)
                        {
                            Console.WriteLine("Le monstre de type " + m.GetType() + " est passé au niveau " + m.Niveau);
                        }
                    }
                }
            });
        }

        public void Ajouter(Monstre m)
        {
            // ajout du montres dans la liste du jeu
            Monstres.Add(m);

            // abonnement
            MonterNiveauHandler += m.Monter;
        }
    }
}
