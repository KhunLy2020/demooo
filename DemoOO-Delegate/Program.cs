﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DemoOO_Delegate
{
    class Program
    {
        static void Main(string[] args)
        {
            Game g = new Game();
            Loup l1 = new Loup();
            Dragon d1 = new Dragon();
            Dragon d2 = new Dragon();
            g.Ajouter(l1);
            g.Ajouter(d1);
            //MonDelegate variable;
            //variable = IsPair;
            //variable += IsImpair;
            //variable.Invoke(7);

            //List<int> list = new List<int> { 1, 2, 3, 4, 5, 6 };
            ////List<int> list2 = Filtrer(list, IsImpair);
            //List<int> list2 = Filtrer(list, x => {
            //    if (x % 3 == 0) return true;
            //    return false;
            //});

            //foreach (int item in list2)
            //{
            //    Console.WriteLine(item);
            //}
            Console.ReadKey();
        }

        static bool IsPair(int nb)
        {
            if (nb % 2 == 0)
                return true;
            return false;
        }

        static bool IsImpair(int nb)
        {
            if (nb % 2 != 0)
                return true;
            return false;
        }

        static List<int> Filtrer(List<int> nombres, MonDelegate function)
        {
            List<int> result = new List<int>();
            foreach(int nb in nombres)
            {
                if (function.Invoke(nb))
                {
                    result.Add(nb);
                }
            }
            return result;
        }
    }
}
