﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DemoOO_3_ClassePartial
{
    // partial pour pouvoir ajouter des membres à la classe
    partial class User
    {
        public string Nom { get; set; }
        public string Prenom { get; set; }
    }
}
