﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DemoOO_3_ClassePartial
{
    class Program
    {
        static void Main(string[] args)
        {
            User u = new User();

            // Prop définie dans la première partie
            u.Nom = "Ly";

            // Prop définie dans la seconde partie
            u.Email = "lykhun@gmail.com";
        }
    }
}
