﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DemoOO_3_ClassePartial
{
    partial class User
    {
        public string Email {  get;  set; }

        //impossible de créer cette propriété car elle existe déjà dans l'autre classe.
        //public string Nom { get; set; }
    }
}
