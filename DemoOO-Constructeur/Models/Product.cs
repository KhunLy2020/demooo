﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DemoOO_Constructeur.Models
{
    class Product
    {
        private string _nom;
        private double _prix;

        public Product(string nom, double prix)
        {
            Nom = nom;
            Prix = prix;
        }

        public string Nom
        {
            get
            {
                return _nom;
            }

            set
            {
                if (string.IsNullOrWhiteSpace(value))
                {
                    //déclencher erreur
                }
                _nom = value;
            }
        }

        public double Prix
        {
            get
            {
                return _prix;
            }

            set
            {
                _prix = value;
            }
        }
    }
}
