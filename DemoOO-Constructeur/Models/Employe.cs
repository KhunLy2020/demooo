﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DemoOO_Constructeur.Models
{
    class Employe : Person
    {
        public string Matricule { get; set; }
        public Employe(string m, string name, int h, int w) : base(name, h, w)
        {
            Matricule = m;
        }
    }
}
