﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DemoOO_Constructeur.Models
{
    class Pokemons
    {
        public int Count { get; set; }
        public string Next { get; set; }
        public string Previous { get; set; }
        public List<Result> Results { get; set; }
    }

    class Result
    {
        public string Name { get; set; }
        public string Url { get; set; }
    }
}
