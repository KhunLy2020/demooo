﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DemoOO_Constructeur.Models
{
    class Person
    {
        public string Name { get; set; }
        public DateTime BirthDate { get; set; }
        public int Height { get; set; }
        public int Weight { get; set; }
        public List<string> Panier { get; }

        public Person(string name)
        {
            if (string.IsNullOrWhiteSpace(name))
            {
                // déclencher une erreur
            }
            else
            {
                Name = name;
            }
        }

        public Person(string name, int h, int w)
        {
            if (string.IsNullOrWhiteSpace(name))
            {
                // déclencher une erreur
            }
            else if(h < 20) {
                // déclencher une erreur
            }
            else if(w < 0) 
            {
                // déclencher une erreur
            }
            else
            {
                Name = name;
                Height = h;
                Weight = w;
                Panier = new List<string>();
            }
        }
        ~Person()
        {
            Console.WriteLine("L'instance est détruite");
        }
    }
}
