﻿using DemoOO_Constructeur.Models;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;

namespace DemoOO_Constructeur
{
    class Program
    {
        static void Main(string[] args)
        {
            Person p = new Person("Khun", 170, 70);
            Person p3 = new Person("Caroline");
            Employe p2 = new Employe("007","Mike", 185, 85);
            Console.WriteLine(p.BirthDate);
            Console.WriteLine(p2.Matricule);
            Console.ReadKey();

            using (FileStream fs = File.Create("test.txt"))
            {
                // ici mon fichier n'est pas n'est fermé
            }
            // ici mon fichier est fermé et est disposable
            File.WriteAllText("test.txt", "salut");


            using (HttpClient client = new HttpClient())
            {
                HttpResponseMessage m = client.GetAsync("https://pokeapi.co/api/v2/pokemon").Result;
                string rep = m.Content.ReadAsStringAsync().Result;
                Pokemons pokemons = JsonConvert.DeserializeObject<Pokemons>(rep);
                foreach (Result item in pokemons.Results)
                {
                    Console.WriteLine(item.Name);
                }
            }
            Console.ReadKey();
        }
    }
}
