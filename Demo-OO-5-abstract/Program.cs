﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Demo_OO_5_abstract
{
    class Program
    {
        static void Main(string[] args)
        {
            // impossible car classe abstraite
            //Produit p = new Produit();

            Produit biere = new ProduitAlcoolise();
            biere.Price = 2;

            Produit mascara = new ProduitDeBeaute();
            mascara.Price = 2;
            Console.WriteLine(biere.GetPriceWithReduction());
            Console.WriteLine(mascara.GetPriceWithReduction());

            Console.ReadKey();
        }
    }
}
