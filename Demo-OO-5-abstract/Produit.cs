﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Demo_OO_5_abstract
{
    abstract class Produit
    {
        public double Price { get; set; }

        protected abstract int GetReduction();

        public double GetPriceWithReduction()
        {
            return Price - (Price * GetReduction() / 100);
        }
    }

    class ProduitDeBeaute : Produit
    {
        protected override int GetReduction()
        {
            return 20;
        }
    }


    class ProduitAlcoolise : Produit
    {
        protected override int GetReduction()
        {
            return 15;
        }
    }
}
