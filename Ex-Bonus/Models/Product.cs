﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Ex_Bonus.Models
{
    class Product
    {

        public string Name { get; set; }

        private double _price;

        public double Price
        {
            get { return _price; }
            set 
            { 
                if(value <= 0)
                {
                    Console.WriteLine("Le prix ne peut être inf ou égale à 0");
                    return;
                }
                _price = value; 
            }
        }

        private int _quantity;

        public int Quantity
        {
            get { return _quantity; }
            set 
            {
                if (value <= 0)
                {
                    Console.WriteLine("La Quantité ne peut être inf ou égale à 0");
                    return;
                }
                _quantity = value; 
            }
        }
    }
}
