﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Ex_Bonus.Models
{
    class Customer
    {
        public string LastName { get; set; }
        public string FirstName { get; set; }
        public List<Product> Basket { get; private set; }
            = new List<Product>();

        public void Add(Product p)
        {
            Basket.Add(p);
        }

        public void Remove(Product p)
        {
            if (!Basket.Remove(p))
            {
                Console.WriteLine("Le produit que vous souhaitez retirer ne se trouve pas ds votre panier");
            }
        }

        public double GetTotal()
        {
            double total = 0;
            foreach (Product item in Basket)
            {
                total += item.Price * item.Quantity;
            }
            return total;
        }

    }
}
