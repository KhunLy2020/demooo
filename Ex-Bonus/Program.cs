﻿using Ex_Bonus.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Ex_Bonus
{
    class Program
    {
        static void Main(string[] args)
        {
            Customer client = new Customer();
            Console.WriteLine("Entrer votre nom puis votre prénom");
            string nom = Console.ReadLine();
            string prenom = Console.ReadLine();

            client.FirstName = prenom;
            client.LastName = nom;


            while (true)
            {
                Console.Clear();
                Console.WriteLine($"Bienvenue {client.LastName} {client.FirstName}");
                Console.WriteLine("Etat du panier: ");
                foreach(Product p in client.Basket)
                {
                    Console.WriteLine($"{p.Quantity}\t{p.Name}\t{p.Price}");
                }
                Console.WriteLine("Total: " + client.GetTotal());

                Console.WriteLine("1. Ajouter un produit");
                Console.WriteLine("2. Supprimer un produit");

                int choixMenu = int.Parse(Console.ReadLine());
                if(choixMenu == 1)
                {
                    Product newP = new Product();
                    Console.WriteLine("Entrez le nom");
                    string nomP = Console.ReadLine();
                    Console.WriteLine("Entrez la quantité");
                    int qt = int.Parse(Console.ReadLine());
                    Console.WriteLine("Entrez le prix unitaire");
                    double prix = double.Parse(Console.ReadLine());

                    newP.Name = nomP;
                    newP.Price = prix;
                    newP.Quantity = qt;

                    client.Add(newP);

                }
                else if(choixMenu == 2)
                {
                    Console.WriteLine("Quel produit voulez supprimer ?");
                    string nomDuProduitASupprimer = Console.ReadLine();
                    List<Product> toDelete = new List<Product>();
                    foreach (Product item in client.Basket)
                    {
                        if(item.Name == nomDuProduitASupprimer)
                        {
                            toDelete.Add(item);
                        }
                    }
                    //if(toDelete != null)
                    {
                        foreach(Product toDel in toDelete)
                        {
                            client.Remove(toDel);
                        }
                    }
                }
            }
        }
    }
}
