﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DemoOO_4_Heritage.Models
{
    class Employe: Personne
    {

        public string NumeroEmploye { get; set; }

        public void RemplirLesRayons()
        {

        }

        public virtual double CalculSalaire ()
        {
            return 2000;
        }

        public void SePresenter()
        {
            Console.WriteLine("Je suis un employé");
        }

        public void SePresenter(string nom)
        {
            Console.WriteLine("Je suis un employe et je m'appelle " + nom);
        }
    }
}
