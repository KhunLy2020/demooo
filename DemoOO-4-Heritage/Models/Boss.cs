﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DemoOO_4_Heritage.Models
{
    class Boss: Employe
    {
        public double PrimeDuBoss { get; set; }

        public override double CalculSalaire()
        {
            return base.CalculSalaire() + PrimeDuBoss;
        }

        public new void SePresenter()
        {
            Console.WriteLine("Je suis un Boss");
        }


    }
}
