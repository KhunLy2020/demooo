﻿using DemoOO_4_Heritage.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DemoOO_4_Heritage
{
    class Program
    {
        static void Main(string[] args)
        {

            Client c = new Client();

            c.SePresenter();

            Employe e1 = new Employe();
            Employe e2 = new Boss() { PrimeDuBoss = 500 };

            Console.WriteLine("Salaire e1 :" + e1.CalculSalaire());
            Console.WriteLine("Salaire e2 :" + e2.CalculSalaire());


            e1.SePresenter();

            e1.SePresenter("Khun");

            e2.SePresenter();
            (e2 as Employe).SePresenter();

            //Personne p = new Personne();
            //p.Nom = "Ly";
            //Client c = new Client();
            //// La propriété nom est héritée de la classe Personne
            //c.Nom = "Person";
            //Console.WriteLine(c is Client); //true

            //Console.WriteLine(p.ToString());

            //// Polymorphisme des classes
            //Console.WriteLine(c is Personne); //true
            //Console.WriteLine(c is Object); //true
            //Console.WriteLine(c is Employe); //false

            //// cast implicite
            //Personne p1 = new Client();

            //Console.WriteLine("p1 est il un client :");
            //Console.WriteLine(p1 is Client);
            //// p1.NumeroClient nest plus possible
            //// cast explicite

            //// avant un cast explicite il vaut mieux vérifier grace au is
            ////if(p1 is Client)
            ////{
            ////    Client c1 = (Client)p1;
            ////    c1.NumeroClient = "0001";
            ////}

            //Client c1 = p1 as Client;
            //c1.NumeroClient = "0001";
            //Console.WriteLine(c1?.NumeroClient ?? "pas de numero");


            //Personne p2 = new Personne();
            //Console.WriteLine("p2 est il un client :");
            //Console.WriteLine(p2 is Client);


            ////if(p2 is Client)
            ////{
            ////    Client c3 = (Client)p2;
            ////}

            //// cast + if
            //Client c3 = p2 as Client;
            ////Console.WriteLine(c3?.NumeroClient ?? "pas de numero");
            //if(c3 == null)
            //    Console.WriteLine("Pas de numero");
            //else
            //    Console.WriteLine(c3.NumeroClient);

            Console.ReadKey();
        }
    }
}
