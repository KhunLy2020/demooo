﻿using Banque.Exceptions;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Banque.Models
{
    class Epargne : Compte
    {
        public Epargne(string numero, Personne titulaire) : base(numero, titulaire)
        {
        }

        public Epargne(string numero, Personne titulaire, double solde) : base(numero, titulaire, solde)
        {
        }

        public DateTime DateDernierRetrait { get; private set; }

        public override void Retrait(double montant)
        {
            if(Solde - montant < 0)
            {
                throw new SoldeInsuffisantException();
            }
            else
            {
                // exécuter ce que fait la classe de base
                base.Retrait(montant);
                // faire qqchose en plus 
                DateDernierRetrait = DateTime.Now;
            }
        }

        protected override double CalculInteret()
        {
            return Solde * 0.045;
        }
    }
}
