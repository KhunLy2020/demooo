﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Banque.Models
{
    interface ICustomer
    {
        double Solde { get; }
        void Retrait(double montant);
        /// <summary>
        /// 
        /// </summary>
        /// <param name="montant"></param>
        /// <exception cref="Exception"></exception>
        void Depot(double montant);
    }
}
