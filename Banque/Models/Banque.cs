﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Banque.Models
{
    class Banque
    {
        public string Nom { get; set; }

        public Dictionary<string, Compte> Comptes { get; private set; }
            = new Dictionary<string, Compte>();

        public void Ajouter(Compte c)
        {
            if (Comptes.ContainsKey(c.Numero))
            {
                Console.WriteLine("Le numero de compte existe déjà");
            }
            else
            {
                Comptes.Add(c.Numero, c);
            }
        }

        public void Retirer(string numero)
        {
            if (!Comptes.ContainsKey(numero))
            {
                Console.WriteLine("Ce compte n'existe pas");
            }
            else
            {
                Comptes.Remove(numero);
            }
        }

        public Compte Rechercher(string numero)
        {
            if (!Comptes.ContainsKey(numero))
            {
                Console.WriteLine("Ce compte n'existe pas");
                return null;
            }
            else
            {
                Compte result = Comptes[numero];
                return result;
            }
        }

    }
}
