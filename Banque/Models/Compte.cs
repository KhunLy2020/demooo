﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Banque.Models
{
    public abstract class Compte : ICustomer, IBanker
    {
        private double _solde;

        public string Numero { get;  private set; }

        public Personne Titulaire { get; private set; }

        public double Solde
        {
            get
            {
                return _solde;
            }

            private set
            {
                _solde = value;MonterNiveauHandler
            }
        }

        public Compte(string numero, Personne titulaire)
        {
            Numero = numero;
            Titulaire = titulaire;
        }

        public Compte(string numero, Personne titulaire, double solde)
        {
            Numero = numero;
            Titulaire = titulaire;
            Solde = solde;
        }

        //public Compte(string numero, Personne titulaire, double solde): this(numero, titulaire)
        //{
        //    Solde = solde;
        //}

        public virtual void Retrait(double montant)
        {
            if (montant <= 0)
            {
                Console.WriteLine("Le Montant retirer ne peut pas être inf ou égal à 0");
            }
            else
            {
                Solde -= montant;
            }
            
        }
        
        
        public void Depot(double montant)
        {
            if (montant <= 0)
            {
                //Console.WriteLine("Le Montant retirer ne peut pas être inf ou égal à 0");
                throw new ArgumentOutOfRangeException("montant", "Le montant ne peut pas etre inf à 0");
            }
            else
            {
                Solde += montant;
            }
        }

        protected abstract double CalculInteret();


        public void AppliquerInteret()
        {
            Solde += CalculInteret();
        }
    }
}
