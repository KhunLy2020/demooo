﻿using Banque.Exceptions;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Banque.Models
{
    class Courant: Compte
    {
        private double _ligneCredit;

        public Courant(string numero, Personne titulaire, double ligneCredit) : base(numero, titulaire)
        {
            LigneCredit = ligneCredit;
        }

        public Courant(string numero, Personne titulaire, double solde, double ligneCredit) : base(numero, titulaire, solde)
        {
            LigneCredit = ligneCredit;
        }

        public double LigneCredit
        {
            get
            {
                return _ligneCredit;
            }

            private set
            {
                if(value >= 0)
                {
                    _ligneCredit = value;
                }
                else
                {
                    throw new InvalidOperationException();
                }
            }
        }

        public override void Retrait(double montant)
        {
            if (Solde - montant < _ligneCredit * -1)
            {
                throw new SoldeInsuffisantException();
            }
            else
            {
                base.Retrait(montant);
            }
        }

        protected override double CalculInteret()
        {
            if(Solde > 0)
            {
                return Solde * 0.03;
            }

            else
            {
                return Solde * 0.0975;
            }
        }
    }
}
