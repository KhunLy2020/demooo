﻿using Banque.Exceptions;
using Banque.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Banque
{
    class Program
    {
        static void Main(string[] args)
        {
            Models.Banque b = new Models.Banque() { 
                Nom = "Fortis"
            };


            // initilisation d'un client
            Personne p1 = new Personne
            (
                "Ly",
                "Khun",
                new DateTime(1982, 5, 6)
            );

            // initialisation d'un compte
            Courant c1 = new Courant
            (
                "BE001...",
                p1,
                1000
            );

            Epargne c2 = new Epargne
                (
                    "BE700...",
                    p1
                );


            b.Ajouter(c1);
            b.Ajouter(c2);
            //b.Ajouter(new Courant { Numero = "BE001..." });

            //b.Retirer("BE300...");
            //b.Retirer("BE700...");

            while (true)
            {
                Console.WriteLine("1. Supprimer");
                Console.WriteLine("2. Transction");
                int ch = int.Parse(Console.ReadLine());
                if(ch == 1)
                {
                    Console.WriteLine("Entrer le numero du compte ...");
                    string numero = Console.ReadLine();
                    b.Retirer(numero);
                }
                else if(ch == 2)
                {
                    Console.WriteLine("Entrer le numero du compte ...");
                    string numero = Console.ReadLine();
                    IBanker compte = b.Rechercher(numero);
                    if (compte != null)
                    {
                        bool pasFini = true;
                        while (pasFini)
                        {
                            Console.WriteLine($"Etat du compte {compte.Numero}:");
                            Console.WriteLine($"Solde: {compte.Solde}");
                            Console.WriteLine("1. Retrait");
                            Console.WriteLine("2. Dépot");
                            Console.WriteLine("3. Ajouter les interets");
                            Console.WriteLine("4. Finir");
                            int choix = int.Parse(Console.ReadLine());
                            Console.Clear();
                            switch (choix)
                            {
                                case 1:
                                    Console.WriteLine("Quel montant souhaitez vous retirer ?");
                                    double montant = double.Parse(Console.ReadLine());
                                    try
                                    {
                                        compte.Retrait(montant);
                                        if (compte is Epargne)
                                        {
                                            Console.WriteLine("Date Retrait :" + (compte as Epargne).DateDernierRetrait);
                                        }
                                    }
                                    catch(SoldeInsuffisantException e)
                                    {
                                        Console.WriteLine(e.Message);
                                    }
                                    
                                    break;
                                case 2:
                                    Console.WriteLine("Quel montant souhaitez vous déposer ?");
                                    double montant2 = double.Parse(Console.ReadLine());
                                    try
                                    {
                                        compte.Depot(montant2);
                                    }
                                    catch (ArgumentOutOfRangeException e)
                                    {
                                        Console.WriteLine(e.Message);
                                    }
                                    break;
                                case 3:
                                    compte.AppliquerInteret();
                                    break;
                                case 4:
                                    pasFini = false;
                                    break;
                            }

                        } 
                    }
                }
            }
        }
    }
}
