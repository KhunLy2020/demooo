﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Banque.Exceptions
{
    class SoldeInsuffisantException: Exception
    {
        public SoldeInsuffisantException() :base("Votre solde est insuffisant") { }
    }
}
