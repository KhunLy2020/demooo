﻿using DemoOO_Exception.Exceptions;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DemoOO_Exception.Models
{
    class PetitPoney
    {
        private string _color;

        private int age;

        public PetitPoney(string color, int age)
        {
            Color = color;
            Age = age;
        }

        public string Color
        {
            get
            {
                return _color;
            }

            set
            {
                if(value != "rose" && value != "jaune" && value != "bleu ciel")
                {
                    throw new ColorException();
                }
                _color = value;
            }
        }

        public int Age
        {
            get
            {
                return age;
            }

            set
            {
                if (value < 0)
                    throw new AgeException("trop petit");
                else if (value > 10)
                    throw new AgeException("trop grand");
                age = value;
            }
        }
    }
}
