﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DemoOO_Exception.Exceptions
{
    class AgeException: Exception
    {
        public AgeException(string m): base(m)
        {

        }
    }
}
