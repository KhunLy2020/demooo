﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DemoOO_Exception.Exceptions
{
    class ColorException: Exception
    {
        public ColorException(): base("La Couleur n'est pas correcte")
        {
        }
    }
}
