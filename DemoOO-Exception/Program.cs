﻿using DemoOO_Exception.Exceptions;
using DemoOO_Exception.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DemoOO_Exception
{
    class Program
    {
        static void Main(string[] args)
        {
            bool badEntry = true;
            int nb = 0;
            while (badEntry)
            {
                try
                {
                    Console.WriteLine("Entrez la couleur de votre poney (rose, jaune, bleu ciel)");
                    string color = Console.ReadLine();
                    Console.WriteLine("Age?");
                    int age = int.Parse(Console.ReadLine());
                    PetitPoney poney = new PetitPoney(color, age);
                    Console.WriteLine($"La couleur de votre poney est {poney.Color}");
                    badEntry = false;
                }
                catch (ColorException e)
                {
                    Console.Clear();
                    Console.ForegroundColor = ConsoleColor.Red;
                    Console.WriteLine(e.Message);
                    Console.ForegroundColor = ConsoleColor.White;
                }
                catch (AgeException e)
                {
                    Console.Clear();
                    Console.ForegroundColor = ConsoleColor.Blue;
                    Console.WriteLine(e.Message);
                    Console.ForegroundColor = ConsoleColor.White;
                }
                catch (Exception e)
                {
                    Console.Clear();
                    Console.WriteLine(e.GetType());
                    Console.ForegroundColor = ConsoleColor.Green;
                    Console.WriteLine(e.Message);
                    Console.ForegroundColor = ConsoleColor.White;
                }
                finally {
                    nb++;
                }
            }
            Console.WriteLine("Nombre total de tentative: " + nb);
            Console.ReadKey();

        }

        static bool TryParse(string val, out int nb)
        {
            try
            {
                nb = int.Parse(val);
                return true;
            }
            catch(Exception)
            {
                nb = default(int);
                return false;
            }
        }
    }
}
