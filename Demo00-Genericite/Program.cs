﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Demo00_Genericite
{

    delegate bool Predidate<T>(T nb);
    delegate bool Predidate(int nb);
    class Program
    {
        static void Main(string[] args)
        {
            List<int> l = new List<int> { 1, 2, 3, 4, 5, 6, 7 };

            List<string> l2 = new List<string> { "papa", "maman", "papy" };

            Filtrer<int>(l, x => { return x % 2 == 0; });

            Filtrer<string>(l2, x => { return x.StartsWith("p"); });


            List<int> l3 = new List<int>();

            Console.ReadKey();
        }


        public static void Filtrer<T>(List<T> l, Predidate<T> f)
            // where T : new() // ajouter une règle sur la class T
        {
            foreach(T item in l)
            {
                if(f(item))
                {
                    Console.WriteLine(item);
                }
            }
        }

        public static void Filter(List<int> l, )
    }
}
