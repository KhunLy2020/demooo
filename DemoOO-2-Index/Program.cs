﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DemoOO_2_Index
{
    class Program
    {
        static void Main(string[] args)
        {
            Ecole ecole = new Ecole();
            Eleve e = new Eleve { Nom = "Ly" };
            Eleve e2 = new Eleve { Nom = "Nathalie" };
            ecole.Inscrire(e);
            ecole.Inscrire(e2);

            // permet de récupérer un élève grace à son matricule
            Console.WriteLine(ecole[e.Matricule]?.Nom);

            //permet de "set" une valeur grace à l'index
            ecole[e.Matricule] = new Eleve();


        }
    }
}
