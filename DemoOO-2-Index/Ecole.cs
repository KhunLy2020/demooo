﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DemoOO_2_Index
{
    class Ecole
    {
        private Dictionary<string, Eleve> _eleves = new Dictionary<string, Eleve>();

        public Eleve this[string key]
        {
            get
            {
                // ce que fera l'instruction : ecole[key]
                if(_eleves.ContainsKey(key))
                {
                    return _eleves[key];
                }
                else
                {
                    return null;
                }
            }
            set 
            { 
                // ce que fera l'instruction : ecole[key] = value
                if(_eleves.ContainsKey(key))
                {
                    Console.WriteLine("Le matricule existe déjà");
                }
                else
                {
                    _eleves.Add(key, value);
                }
            }
        }

        public void Inscrire(Eleve e)
        {
            string matricule = Guid.NewGuid().ToString();
            e.Matricule = matricule;
            _eleves.Add(matricule, e);
        }
    }
}
