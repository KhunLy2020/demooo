﻿using DemoOO_6_Interface.Models;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DemoOO_6_Interface
{
    //class Test : IEnumerable<int>
    //{
    //    public IEnumerator<int> GetEnumerator()
    //    {
    //        throw new NotImplementedException();
    //    }

    //    IEnumerator IEnumerable.GetEnumerator()
    //    {
    //        throw new NotImplementedException();
    //    }
    //}
    class Program
    {
        // polymorphisme
        static ITelephone Telephone = new Android();
        static void Main(string[] args)
        {
            // Test t = new Test();
            // Comme la class Test respecte les conditions pour pouvoir
            // être utilisées dans une boucle foreach
            // on peut l'utiliser dans l'exemple ci dessous
            // foreach(int item in t) { }

            Secretaire monique = new Secretaire();
            monique.tel = Telephone;
            monique.TelephonerAUnFournisseur();
            Comptable charles = new Comptable();
            charles.Tel = Telephone;
            charles.Telephoner();
            Console.ReadKey();
        }
    }
}
