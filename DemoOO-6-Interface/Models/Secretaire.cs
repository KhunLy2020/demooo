﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DemoOO_6_Interface.Models
{
    class Secretaire
    {
        public ITelephone tel { get; set; }
        public void TelephonerAUnFournisseur()
        {
            if(tel == null)
            {
                Console.WriteLine("Je ne peux pas téléphoner");
            }
            else
            {
                tel.Telephoner();
            }
        }
    }
}
