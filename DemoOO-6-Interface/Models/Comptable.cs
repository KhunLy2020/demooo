﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DemoOO_6_Interface.Models
{
    class Comptable
    {
        public ITelephone Tel { get; set; }
        public void Telephoner()
        {
            Tel.Telephoner();
        }
    }
}
