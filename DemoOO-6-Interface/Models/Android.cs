﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DemoOO_6_Interface.Models
{
    class Android: ITelephone
    {
        public void Telephoner()
        {
            Console.WriteLine("Je permets de téléphoner avec un smartphone");
        }

        public void EnvoyerDesMails() 
        {
            Console.WriteLine("J'envoi un email");
        }
    }
}
