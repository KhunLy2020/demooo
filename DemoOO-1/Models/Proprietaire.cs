﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DemoOO_1.Models
{
    public class Proprietaire
    {
        public string Nom { get; set; }

        private DateTime _birthDate;

        public DateTime BirthDate
        {
            get
            {
                return _birthDate;
            }

            set
            {
                if(value > DateTime.Now)
                {
                    Console.WriteLine("On ne peut pas être né avant ajd");
                }
                else{
                    _birthDate = value;
                }
            }
        }
    }
}
