﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DemoOO_1.Models
{
    class Voiture
    {
        #region Variables
        private static int _constanteAcceleration = 15;

        private string _couleur;

        private string _marque;

        private double _vitesse;

        private Proprietaire _proprio;
        #endregion

        #region Propriétés
        public int ConstanteAcceleration
        {
            get
            {
                return _constanteAcceleration;
            }
        }

        public double Vitesse
        {
            get
            {
                return _vitesse;
            }
            set
            {
                if (value < 0)
                {
                    //throw new Exception("Vous ne pouvrez mettre de valeur négative");
                    Console.WriteLine("Vous ne pouvrez mettre de valeur négative");
                }
                else
                {
                    _vitesse = value;
                }
            }
        }

        public string Couleur
        {
            get
            {
                return _couleur;
            }

            set
            {
                _couleur = value;
            }
        }

        public string Marque
        {
            get
            {
                return _marque;
            }

            set
            {
                _marque = value;
            }
        }

        public Proprietaire Proprio
        {
            get
            {
                return _proprio;
            }

            set
            {
                if(value.BirthDate.Year > 2002)
                {
                    Console.WriteLine("On ne peut pas etre propriétaire avant 18 ans");
                }
                else
                {
                    _proprio = value;
                }
            }
        }
        #endregion

        #region Constructeurs et le destructeur

        #endregion

        #region Méthodes
        public void Accelerer()
        {
            this.Vitesse = this.Vitesse + this.ConstanteAcceleration;
        } 

        public static void ToutAccelerer()
        {
            _constanteAcceleration++; ;
        }
        #endregion
    }
}
