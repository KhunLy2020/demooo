﻿using DemoOO_1.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DemoOO_1
{
    class Program
    {
        public static int test = 42;

        static void Main(string[] args)
        {
            Voiture voiture = new Voiture();
            Console.WriteLine(voiture.Vitesse);

            voiture.Couleur = "Rouge";
            voiture.Vitesse = 50;
            voiture.Marque = "Ferrari";
            //voiture.ConstanteAcceleration = 100;

            Console.WriteLine("Vitesse de la voiture 1: " + voiture.Vitesse);

            //instatanciation 
            Voiture voiture2 = new Voiture();

            //affectactions
            voiture2.Couleur = "Bleu";
            voiture2.Vitesse = 50;
            voiture2.Marque = "Peugeot";

            Console.WriteLine("Vitesse de la voiture 2: " + voiture2.Vitesse);

            // instanciation affectactions
            Proprietaire p = new Proprietaire
            {
                Nom = "Ly", BirthDate = new DateTime(1982,5,6)
            };

            voiture2.Proprio = p;

            Console.WriteLine(voiture2.Proprio?.Nom);

            
            Voiture.ToutAccelerer();
            voiture.Accelerer();
            voiture2.Accelerer();

            Console.WriteLine("La voiture 1 accélère");

            Console.WriteLine("Vitesse de la voiture 1: " + voiture.Vitesse);

            Console.WriteLine("Vitesse de la voiture 2: " + voiture2.Vitesse);

            Console.ReadKey();


            string mot = "salut";
            List<string> liste = new List<string>();
            liste.Add(mot);
            liste.Add("test");

            Voiture v1 = new Voiture { Couleur = "Bleu" };
            new Voiture { Couleur = "Rouge" };
            List<Voiture> liste2 = new List<Voiture>();
            liste2.Add(new Voiture { Couleur = "Rouge" });
            liste2.Add(new Voiture { Couleur = "Rouge" });

            Voiture nomVarialble = liste2[0];
        }
    }
}
